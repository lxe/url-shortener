import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {url: '',
    shortURL: '',
    redirectURL: ''};
  }

  handleChange(event) {
    this.setState({url: event.target.value});;
  }

  handleButtonClick(event) {
    event.preventDefault();
    fetch(`/add/${this.state.url}`)
    .then((res) => {
      return res.json();
    })
    .then((json) => {
      this.setState({shortURL: json.shortURL});
      this.setState({redirectURL: json.redirectURL});
    })
    .catch(error => console.error('Error:', error));
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Chen's URL Shortener</h1>
        </header>
        <div className="App-intro">
          <form onSubmit={e => this.handleButtonClick(e)}>
            <input type="text" placeholder="Enter URL" onChange={e => this.handleChange(e)} />
            <button>Shorten</button>
          </form>
          <div>{this.state.shortURL}</div>
          <a href={this.state.redirectURL}>{this.state.redirectURL}</a>
        </div>
      </div>
    );
  }
}

export default App;
