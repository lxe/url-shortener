// implement persistence, logic, etc...

// https://www.npmjs.com/package/sqlite3

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database("./db");

// db.serialize(function() {
  db.run("CREATE TABLE IF NOT EXISTS urls (info TEXT)");

  // db.each("SELECT rowid AS id, info FROM urls", function(err, row) {
  //     console.log(row.id + ": " + row.info);
  // });
// });

// db.close();


function addURL(url) {
  return new Promise(function(resolve, reject) {
    db.run("INSERT INTO urls VALUES (?)", [url], function(err) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        var shortURL = getShortURL(this.lastID); // rowID -> shortURL
        //console.log('rowID:', this.lastID,  '-> shortURL:', shortURL);
        resolve(shortURL);
      }
    });
  });
}

function getURL(shortURL) {
  var rowID = getRowID(shortURL); // shortURL -> rowID
  //console.log('shorURL:', shortURL,  '-> rowID:', rowID);
  return new Promise(function(resolve, reject) {
    db.get(`SELECT info FROM urls where rowid == ${rowID}`, (err, row) => {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

// encoding/decoding
function getShortURL(rowID) {
  return (rowID).toString(36);
}
function getRowID(shortURL) {
  return parseInt(shortURL,36);
}

module.exports = app => {

  // Navigate the short link (ID) and redirect
  app.all("/:shortUrl", (req, res) => {         // TODO: update regex to accept characters /\/\d+$/
    const shortURL = req.params.shortUrl;       //req.url.slice(1);

    getURL(shortURL)
      .then(row => {
        res.writeHead(302, {
          Location: row.info
        });
        res.end(`Redirecting to ${row.info}...`);
      })
      .catch(e => res.json(e));
  });

  // Add a URL and get back shortURL
  app.all("/add/*", (req, res) => {
    addURL(req.params[0])
    .then(shortURL => {
      //respond with redirectURL and shortURL
      var redirectURL = 'http://' + req.headers.host + '/' + shortURL;
      res.json({shortURL: shortURL, redirectURL: redirectURL})
    })
    .catch(e => res.json(e));
  });
};
